from django.urls import path

from . import views

app_name = 'grouptalks'
base_path = 'threads/<int:thread_pk>/'
urlpatterns = [path(base_path,
                    views.ThreadView.as_view(),
                    name=views.ThreadView.pattern_name),
               path(base_path+'reply/',
                    views.ReplyView.as_view(),
                    name=views.ReplyView.pattern_name),]
