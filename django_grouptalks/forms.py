from django.forms import (BooleanField, CharField, CheckboxInput, ChoiceField,
                          HiddenInput, IntegerField, ModelChoiceField,
                          Textarea,
                          ModelForm, TextInput, ModelMultipleChoiceField,
                          ValidationError)
#from django.utils.html import format_html
from django_select2.forms import ModelSelect2MultipleWidget
from django.conf import settings

from .models import Message, Thread


class ThreadSimpleCreateForm(ModelForm):
    """ Simple form with only body and predifined hidden subject """
    subject = CharField(widget=HiddenInput(), required=True)
    body = CharField(widget=Textarea(attrs={'rows':1, 'cols': 18}), required=True)

    class Meta:
        model = Thread
        fields = ['subject', 'body']

class MessageForm(ModelForm):
    """ simple message form """
    default_textarea_attrs = {'cols': 80, 'rows': 4}

    body = CharField(widget=Textarea(attrs=getattr(settings,
                                                   'GROUPTALKS_MSG_BODY_ATTRS',
                                                   default_textarea_attrs)))

    class Meta:
        model = Message
        fields = ['body',]
