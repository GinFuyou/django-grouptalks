# Generated by Django 2.0.8 on 2018-08-22 14:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_datetime', models.DateTimeField(default=django.utils.timezone.localtime, editable=False)),
                ('change_datetime', models.DateTimeField(auto_now=True)),
                ('body', models.TextField(max_length=8192)),
                ('is_delisted', models.BooleanField(default=False, verbose_name='Is delisted')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_message', to=settings.AUTH_USER_MODEL, verbose_name='Creator')),
            ],
            options={
                'db_table': 'grouptalks_messages',
                'verbose_name': 'Message',
            },
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_datetime', models.DateTimeField(default=django.utils.timezone.localtime, editable=False)),
                ('change_datetime', models.DateTimeField(auto_now=True)),
                ('body', models.TextField(max_length=8192)),
                ('is_delisted', models.BooleanField(default=False, verbose_name='Is delisted')),
                ('subject', models.CharField(max_length=140, verbose_name='Subject')),
                ('is_closed', models.BooleanField(default=False, verbose_name='Is closed')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_thread', to=settings.AUTH_USER_MODEL, verbose_name='Creator')),
            ],
            options={
                'db_table': 'grouptalks_threads',
                'verbose_name': 'Thread',
            },
        ),
        migrations.CreateModel(
            name='ThreadParticipant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latest_read_datetime', models.DateTimeField(blank=True, null=True)),
                ('thread', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='django_grouptalks.Thread', verbose_name='Thread')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'db_table': 'grouptalks_thread_participation',
                'verbose_name': 'Thread participation',
            },
        ),
        migrations.CreateModel(
            name='ThreadState',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(max_length=128, unique=True)),
                ('title', models.CharField(blank=True, max_length=128)),
                ('priority', models.PositiveSmallIntegerField(default=200, verbose_name='Priority')),
            ],
            options={
                'db_table': 'grouptalks_threadstates',
                'verbose_name': 'Thread state',
            },
        ),
        migrations.AddField(
            model_name='thread',
            name='participants',
            field=models.ManyToManyField(blank=True, through='django_grouptalks.ThreadParticipant', to=settings.AUTH_USER_MODEL, verbose_name='Participants'),
        ),
        migrations.AddField(
            model_name='thread',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='django_grouptalks.ThreadState', verbose_name='State'),
        ),
        migrations.AddField(
            model_name='message',
            name='thread',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='django_grouptalks.Thread'),
        ),
        migrations.AlterUniqueTogether(
            name='threadparticipant',
            unique_together={('user', 'thread')},
        ),
    ]
