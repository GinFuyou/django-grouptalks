from datetime import date, timedelta

from django.conf import settings
#from django.contrib import messages
from django.contrib.auth import get_user_model, mixins
from django.contrib.auth.decorators import login_required #, permission_required
#from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.utils import timezone
#from django.db.models import Count, F
#from django.forms import modelformset_factory
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.gzip import gzip_page
from django.views.generic import DetailView, edit, list as list_views
from django.utils.translation import gettext_lazy as _
from django.db import transaction

from .models import Thread, ThreadParticipant, Message

from .forms import ThreadSimpleCreateForm, MessageForm


class ThreadMixin(mixins.AccessMixin):
    """ Mixin that retrieves Thread object and checks user participation """
    permission_denied_message = _("You don't have membership in this thread!")
    thread = None
    raise_exception = True
    thread_model = Thread
    pk_url_kwarg = 'thread_pk'

    def dispatch(self, request, *args, **kwargs):
        self.thread = self.get_thread()
        if not self.check_thread_access(self.thread):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_thread(self):
        pk = self.kwargs.get(self.pk_url_kwarg)
        queryset = self.thread_model.objects.filter(pk=pk)
        queryset = queryset.select_related('creator').prefetch_related('participants')
        # NOTE add permission to see delisted
        if not self.request.user.is_superuser:
            queryset = queryset.listed()
        obj = None
        try:
            obj = queryset.get()
        except self.thread_model.ObjectDoesNotExist:
            raise Http404(_("Thread does not exist. Possibly it was removed."))
        return obj

    def check_thread_access(self, thread):
        # add superuser access ?
        if self.request.user in thread.participants.all():
            check_passed = True
        else:
            check_passed = False
        print("Thread access: {0}".format(check_passed))
        #print(repr(thread.participants.all()))
        return check_passed

@method_decorator(login_required, name='dispatch')
class ThreadView(ThreadMixin, DetailView):
    model = Thread
    context_object_name = 'thread'
    pattern_name = 'thread'

    def get_messages(self, delisted=False, ordered=True):
        return Message.objects.by_thread(self.thread.pk, delisted=delisted, ordered=ordered)
    """
    def get_queryset(self):
        queryset = super().get_queryset().prefetch_related('participants')
        queryset = queryset.select_related('creator')
        # NOTE impement flexible checks
        if not self.request.user.is_superuser:
            queryset = queryset.listed()
        return queryset
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # WARNING don't use "messages" to not vconfuse with django messages framework!
        context['thread_messages'] = self.get_messages()
        if not self.thread.is_closed:
            context['reply_form'] = MessageForm()
        return context

@method_decorator(login_required, name='dispatch')
class ThreadCreateView(edit.CreateView):
    model = Thread
    form_class = ThreadSimpleCreateForm
    pattern_name = 'thread_create'
    thread = None

    def get_participants(self):
        """ add extra participants, creator added separately """
        return get_user_model().objects.none()

    def get_success_url(self):
        return self.thread.get_absolute_url()

    def form_valid(self, form):
        with transaction.atomic():
            form.instance.creator = self.request.user
            self.thread = form.save()
            obj_list = [ThreadParticipant(user=self.request.user, thread=self.thread),]
            for user in self.get_participants():
                obj_list.append(ThreadParticipant(user=user, thread=self.thread))
            ThreadParticipant.objects.bulk_create(obj_list)

        return redirect(self.get_success_url())

@method_decorator(login_required, name='dispatch')
class ReplyView(ThreadMixin, edit.CreateView):
    model = Message
    pattern_name = 'reply'
    form_class = MessageForm

    def form_valid(self, form):
        form.instance.thread = self.thread
        form.instance.creator = self.request.user
        msg = form.save(commit=True)
        return redirect(reverse('grouptalks:{0}'.format(ThreadView.pattern_name),
                                kwargs={ThreadView.pk_url_kwarg: self.thread.pk}))
