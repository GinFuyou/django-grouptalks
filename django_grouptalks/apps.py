from django.apps import AppConfig


class DjangoGrouptalksConfig(AppConfig):
    name = 'django_grouptalks'
