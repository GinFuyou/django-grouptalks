from django.contrib import admin

from . import models


@admin.register(models.Thread)
class ThreadAdmin(admin.ModelAdmin):
    list_display = ('subject', 'creator', 'create_datetime', 'change_datetime',
                    'is_delisted', 'state')

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.creator = request.user
        super().save_model(request, obj, form, change)


@admin.register(models.Message)
class MessageAdmin(ThreadAdmin):
    list_display = ('id',
                    'thread',
                    'creator',
                    'create_datetime',
                    'change_datetime',
                    'is_delisted')
