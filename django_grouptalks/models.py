from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy
# WARNING REWORK
#from kern.staff_mailer import send_staff_mail


class BaseMessageQuerySet(models.QuerySet):
    """ Base QuerySet for AbstactBaseMessage """
    def listed(self):
        return self.filter(is_delisted=False)

class MessageQuerySet(BaseMessageQuerySet):
    """ QuerySet for Message """
    def by_thread(self, thread_pk, ordered=True, delisted=False):
        queryset = self.filter(thread__pk=thread_pk, is_delisted=delisted)
        if ordered:
            queryset = queryset.order_by("change_datetime")
        return queryset


class AbstactBaseMessage(models.Model):
    """
    Base class for Message and Thread
    """
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                blank=True,
                                null=True,
                                verbose_name=_("Creator"),
                                related_name="created_%(class)s",
                                on_delete=models.SET_NULL)
    create_datetime = models.DateTimeField(default=timezone.localtime, editable=False)
    change_datetime = models.DateTimeField(auto_now=True)
    body = models.TextField(max_length=8192)
    is_delisted = models.BooleanField(_("Is delisted"), default=False)

    objects = BaseMessageQuerySet.as_manager()

    class Meta:
        abstract = True

class ThreadState(models.Model):
    """
    State oobject for Thread. e.g.: 'important', 'sticky', ...
    """
    slug = models.SlugField(max_length=128, unique=True)
    title = models.CharField(max_length=128, blank=True)
    priority = models.PositiveSmallIntegerField(_('Priority'), default=200)

    class Meta:
        verbose_name = _("Thread state")
        db_table = 'grouptalks_threadstates'

    def __str__(self):
        return self.slug

class Thread(AbstactBaseMessage):
    """
    Messaging thread model, it defines groups of users who can see and write
    messages and tracks last read time
    """
    subject = models.CharField(_("Subject"), max_length=140)
    is_closed = models.BooleanField(_("Is closed"), default=False)
    state = models.ForeignKey('ThreadState',
                              verbose_name=_("State"),
                              blank=True,
                              null=True,
                              on_delete=models.SET_NULL)

    participants = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                          blank=True,
                                          verbose_name=_('Participants'),
                                          through='ThreadParticipant')

    class Meta:
        verbose_name = _("Thread")
        db_table = 'grouptalks_threads'

    def __str__(self):
        subject = self.subject
        if len(subject) > 30:
            subject = subject[:21] + '...'
        if self.creator:
            creator_str = str(self.creator)
        else:
            creator_str = _('N/A')
        return "{0} by ({1})".format(subject, creator_str)

    #def save(self, *args, **kwargs):
        #super().save(*args, **kwargs)s

    def get_absolute_url(self):
        return reverse_lazy('grouptalks:thread', kwargs={"thread_pk": self.pk})

    def get_last(self):
        if self.message_set.exists():
            return self.message_set.latest()
        else:
            return {'body': self.body, 'creator': self.creator, 'create_datetime': self.create_datetime}

class ThreadParticipant(models.Model):
    """
    Through table model between Thread and AUTH_USER_MODEL.
    Keeps data about datetime of last read message
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name=_('User'),
                             on_delete=models.CASCADE)
    thread = models.ForeignKey('Thread', verbose_name=_('Thread'), on_delete=models.CASCADE)
    latest_read_datetime = models.DateTimeField(blank=True, null=True)
    # role NOTE implement later

    class Meta:
        verbose_name = _("Thread participation")
        db_table = 'grouptalks_thread_participation'
        unique_together = (('user', 'thread'),)

class Message(AbstactBaseMessage):
    """
    A message in thread
    """
    thread = models.ForeignKey('Thread', on_delete=models.CASCADE)

    objects = MessageQuerySet.as_manager()

    class Meta:
        verbose_name = _("Message")
        db_table = 'grouptalks_messages'
        get_latest_by = 'create_datetime'
